package com.arrk.execersice.interfaces;

/**
 * Created by Ashish S. Patil on 11-06-2018.
 * App specific Config values
 */
public interface ApplicationConfig {

    /*
    Way Call API method
    */
    int POST = 1;
    int GET = 2;
    /*
    Responce Callback
    */
    int SUCCESS = 200;
    int NOINTERNETCONNECTION = 606;

    /*
    Storing data sharedpref value name
    */
    String SharedPrefName = "arrkAPP";
}
