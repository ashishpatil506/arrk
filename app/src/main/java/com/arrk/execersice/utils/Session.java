package com.arrk.execersice.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.arrk.execersice.interfaces.ApplicationConfig;

/**
 * Created by Ashish S. Patil on 11-06-2018.
 */
public class Session implements ApplicationConfig {
    public static void setStringPref(Context context, String prefName, String data) {
        SharedPreferences prefs = context.getSharedPreferences(SharedPrefName, Context.MODE_PRIVATE);
        prefs.edit().putString(prefName, data).apply();
    }

    public static String getStringPref(Context context, String prefName) {
        SharedPreferences prefs = context.getSharedPreferences(SharedPrefName, Context.MODE_PRIVATE);
        return prefs.getString(prefName, "");
    }

    public static void setBooleanPref(Context context, String prefName, boolean data) {
        SharedPreferences prefs = context.getSharedPreferences(SharedPrefName, Context.MODE_PRIVATE);
        prefs.edit().putBoolean(prefName, data).apply();
    }
    public static void setDatePref(Context context, String prefName, String data) {
        SharedPreferences prefs = context.getSharedPreferences(SharedPrefName, Context.MODE_PRIVATE);
        prefs.edit().putString(prefName, data).apply();
    }
    public static String getDatePref(Context context, String prefName) {
        SharedPreferences prefs = context.getSharedPreferences(SharedPrefName, Context.MODE_PRIVATE);
        return prefs.getString(prefName, "");

    }
    public static boolean getBooleanPref(Context context, String prefName) {
        SharedPreferences prefs = context.getSharedPreferences(SharedPrefName, Context.MODE_PRIVATE);
        return prefs.getBoolean(prefName, false);

    }

    public static void setIntegerPref(Context context, String prefName, int data) {
        SharedPreferences prefs = context.getSharedPreferences(SharedPrefName, Context.MODE_PRIVATE);
        prefs.edit().putInt(prefName, data).apply();
    }

    public static int getIntegerPref(Context context, String prefName) {
        SharedPreferences prefs = context.getSharedPreferences(SharedPrefName, Context.MODE_PRIVATE);
        return prefs.getInt(prefName, 0);
    }


}
