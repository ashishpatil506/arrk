package com.arrk.execersice.utils;

import com.arrk.execersice.interfaces.ApplicationConfig;

/**
 * Created by Ashish S. Patil on 11-06-2018.
 * Storing Config Values here
 */
public class Constants implements ApplicationConfig {
    /*
    // true - if you want to see the Logs and false - will be log free.
    */
    public static final boolean DEBUG = true;
   }
