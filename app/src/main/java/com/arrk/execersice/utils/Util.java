package com.arrk.execersice.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.support.design.widget.Snackbar;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {


    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }






    public static String printRawResponse(Object response) {
        String data = Util.gson().toJson(response);
        Loggers.D("RawData ", "   " + data);
        return data;

    }


    public static Gson gson() {
        return
                new GsonBuilder().serializeNulls()
                        .registerTypeHierarchyAdapter(
                                Collection.class, new CollectionAdapter())
                        .create();
    }

    public static class CollectionAdapter implements JsonSerializer<Collection<?>> {
        @Override
        public JsonElement serialize(Collection<?> src, Type typeOfSrc,
                                     JsonSerializationContext context) {
            if (src == null || src.isEmpty())
                return null;

            JsonArray array = new JsonArray();

            for (Object child : src) {
                JsonElement element = context.serialize(child);
                array.add(element);
            }

            return array;
        }
    }

    public static String setHandleStringException(String values, String defaultValues) {
        if (values == null || values.equalsIgnoreCase("null") || values.length() == 0 || values.replace(" ", "").length() == 0)
            return defaultValues;
        return values;
    }

    public static String getCharacterDate(String timeStamp) {
        String formattedTime;
        Date d;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.getDefault());
            sdf.setTimeZone(TimeZone.getTimeZone(String.valueOf(Calendar.getInstance().getTimeZone())));
            SimpleDateFormat output = new SimpleDateFormat("EEEE, dd MMMM yyyy @hh:mm a", Locale.getDefault());
            d = sdf.parse(timeStamp);
            formattedTime = output.format(d);
            Loggers.E("Util covertTimeDate >>>> ", timeStamp + "  >>>>  " + formattedTime);
            return formattedTime;
        } catch (Exception e) {
            Loggers.E("Util covertTimeDate >>>> ", e.getMessage());
            return "0";
        }
    }

    public static String getinCMToMeter(String cm) {
        float getMeter;
        try {
            getMeter = Float.parseFloat(cm) / 100;
            return String.valueOf(getMeter + " Metre");
        } catch (Exception e) {
            return "";
        }

    }

    public static void enableDisableWifi(Context context,boolean isOn) {
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(isOn);
    }
}