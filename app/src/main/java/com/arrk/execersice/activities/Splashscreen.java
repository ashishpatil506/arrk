package com.arrk.execersice.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import com.arrk.execersice.R;

public class Splashscreen extends ParentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        new countDownStart(3000, 1000).start();

    }

    /*hold Activity for 3 seconds*/
    private class countDownStart extends CountDownTimer {
        public countDownStart(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {

        }

        @Override
        public void onFinish() {
            Intent intent = new Intent(Splashscreen.this, CharacterListingActivity.class);
            startActivity(intent);
            finish();

        }
    }

}
