package com.arrk.execersice.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arrk.execersice.R;
import com.arrk.execersice.interfaces.ApplicationConfig;
import com.arrk.execersice.interfaces.Urls;

/**
 * Created by Ashish S. Patil on 11-06-2018.
 */

public abstract class ParentActivity extends AppCompatActivity implements ApplicationConfig, Urls {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    //Set error view value & display in Layout level
    public View setErrorScreen(View view, int imageid, String title, String message, View.OnClickListener retrylistener, boolean isvisbileretry, String retyrTitle) {
        TextView errortitle, errormessage;
        Button errorretry;
        ImageView erroricon;
        RelativeLayout error_mainLayout;
        /*
         if view use when its call in fragment else set null in Activity*
         */
        if (view != null) {
            ((ImageView) view.findViewById(R.id.erroricon)).setImageResource(imageid);
            errortitle = (TextView) view.findViewById(R.id.errortitle);
            errormessage = (TextView) view.findViewById(R.id.errormessage);
            errorretry = (Button) view.findViewById(R.id.errorretry);
            erroricon = (ImageView) view.findViewById(R.id.erroricon);
            error_mainLayout = (RelativeLayout) view.findViewById(R.id.error_mainLayout);

        } else {
            ((ImageView) findViewById(R.id.erroricon)).setImageResource(imageid);
            errortitle = ((TextView) findViewById(R.id.errortitle));
            errormessage = ((TextView) findViewById(R.id.errormessage));
            ((ImageView) findViewById(R.id.erroricon)).setImageResource(imageid);
            errorretry = ((Button) findViewById(R.id.errorretry));
            erroricon = ((ImageView) findViewById(R.id.erroricon));
            error_mainLayout = ((RelativeLayout) findViewById(R.id.error_mainLayout));
        }
        //Retry button not required the set false
        if (isvisbileretry) {
            errorretry.setVisibility(View.VISIBLE);
            errorretry.setOnClickListener(retrylistener);
        } else errorretry.setVisibility(View.GONE);
        if (retyrTitle != null && retyrTitle.length() > 0)
            errorretry.setText(retyrTitle);
        errortitle.setText(title);
        errormessage.setText(message);
        erroricon.setImageResource(imageid);
        //if Title not available
        if (title.length() > 0)
            errortitle.setVisibility(View.VISIBLE);
        else
            errortitle.setVisibility(View.GONE);

        return error_mainLayout;

    }


}
