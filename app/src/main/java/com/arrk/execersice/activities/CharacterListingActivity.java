package com.arrk.execersice.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.arrk.execersice.R;
import com.arrk.execersice.callBacks.GetCallBackResponce;
import com.arrk.execersice.customClasses.LoadMoreScrollListener;
import com.arrk.execersice.customClasses.ProgressBarView;
import com.arrk.execersice.model.character.CharacterData;
import com.arrk.execersice.model.character.CharacterList;
import com.arrk.execersice.recyclerViewAdapters.CharcterListAdapter;
import com.arrk.execersice.restApi.APIResponce;
import com.arrk.execersice.utils.Loggers;
import com.arrk.execersice.utils.Util;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CharacterListingActivity extends ParentActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView((R.id.swiperefreshlayout))
    SwipeRefreshLayout swiperefreshlayout;
    private LinearLayoutManager linearLayoutManager;


    @BindView(R.id.progressbarview)
    ProgressBarView progressbarview;


    private View error_view;
    private int paginationCount = 1;
    private APIResponce getAPIResponce;
    private HashMap<String, Object> body = new HashMap<>();
    private ArrayList<CharacterData> characterDataArrayList = new ArrayList<CharacterData>();
    //if Remove All List Data the true else false
    private boolean isRemoveALL = false;
    private String characterApiPath = null;

    private CharcterListAdapter charcterAdapter;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_listing);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle(R.string.charcterTitle);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);


        recyclerView.addOnScrollListener(new LoadMoreScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMoreData(int page, int totalItemsCount) {
                callAPI(false);
            }

            @Override
            public void onMoved(int distance) {


            }

            @Override
            public void onShow() {


            }

            @Override
            public void onHide() {

            }
        });

        callAPI(false);
    }

    /*
    *
    call APi to fetch data
    *
    */
    private void callAPI(boolean isRemoveALL) {
        body = new HashMap<>();
        this.isRemoveALL = isRemoveALL;

        if (isRemoveALL) {
            crearData();
            characterApiPath = null;
        }
        if (characterDataArrayList.size() != 0) {
            progressbarview.setVisibility(View.GONE);
            swiperefreshlayout.setRefreshing(true);
        } else {
            progressbarview.setVisibility(View.VISIBLE);
        }
        if (error_view != null)
            error_view.setVisibility(View.GONE);
        if (characterApiPath == null)
            characterApiPath = characterAPI;

        getAPIResponce = new APIResponce(this, "", characterApiPath, "characterApiPath", body, new GetCallBackResponce() {
            @Override
            public void getCallBackResponce(int code, String message, String Tag, JSONObject jsonObject) {

                Loggers.D("jsonObject ", jsonObject + "");

                if (code == SUCCESS) {

                    try {

                        CharacterList characterData = Util.gson().fromJson(jsonObject.toString(), CharacterList.class);
                        if (!characterData.getCharacterData().isEmpty()) {
                            characterDataArrayList.addAll(characterData.getCharacterData());
                            characterApiPath = characterData.getNextPath();
                            setData();
                        }
                        if (characterData.getCharacterData().size() > 0)
                            paginationCount++;
                        else if (characterDataArrayList.size() == 0) {
                            crearData();
                            callNoData(getString(R.string.noDataTitle));

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        callNoData(getString(R.string.commonerror));
                    }
                    progressbarview.setVisibility(View.GONE);
                    swiperefreshlayout.setRefreshing(false);
                } else if (code == NOINTERNETCONNECTION) {

                    if (characterDataArrayList.size() == 0) {
                        callNoInternet();
                    }
                    progressbarview.setVisibility(View.GONE);
                    swiperefreshlayout.setRefreshing(false);
                } else if (code == NOINTERNETCONNECTION) {
                    if (characterDataArrayList.size() == 0) {
                        callNoInternet();
                    }
                    progressbarview.setVisibility(View.GONE);
                    swiperefreshlayout.setRefreshing(false);
                } else {
                    if (characterDataArrayList.size() == 0) {
                        callNoData(getString(R.string.commonerror));
                    }
                    progressbarview.setVisibility(View.GONE);
                    swiperefreshlayout.setRefreshing(false);
                }
            }
        }, GET);
    }

    /*
       call  when internet not available
   */
    private void callNoInternet() {
        error_view = setErrorScreen(null, R.drawable.nointernet, getString(R.string.noInternetTitle), getString(R.string.noInternetmgs), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAPI(isRemoveALL);
            }
        }, true,null);
        error_view.setVisibility(View.VISIBLE);
    }

    /*
    call  when Data not found
    */
    private void callNoData(String message) {
        error_view = setErrorScreen(null, 0, message, getString(R.string.empty_string), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAPI(isRemoveALL);
            }
        }, true,null);
        error_view.setVisibility(View.VISIBLE);
    }

    /*
     set Data to the Adapter
        */
    private void setData() {
        swiperefreshlayout.setRefreshing(false);
        if (charcterAdapter != null) {
            charcterAdapter.notifyDataSetChanged();
        } else {
            charcterAdapter = new CharcterListAdapter(this, characterDataArrayList);

            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(charcterAdapter);
        }


    }

    /*
     *
     For Clear Data
     *
     */
    public void crearData() {
        paginationCount = 1;
        characterDataArrayList.removeAll(characterDataArrayList);
        setData();
    }


}