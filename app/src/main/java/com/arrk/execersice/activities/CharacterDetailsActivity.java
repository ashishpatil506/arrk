package com.arrk.execersice.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.arrk.execersice.R;
import com.arrk.execersice.model.character.CharacterData;
import com.arrk.execersice.utils.Util;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CharacterDetailsActivity extends ParentActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvCharacterName)
    TextView tvCharacterName;
    @BindView(R.id.tvCharacterHeight)
    TextView tvCharacterHeight;
    @BindView(R.id.tvCharacterMass)
    TextView tvCharacterMass;
    @BindView(R.id.tvCreatedDate)
    TextView tvCreatedDate;


    private CharacterData characterData;


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        characterData = (CharacterData) getIntent().getSerializableExtra("characterData");
        setTitle(characterData.getName());
        tvCharacterName.setText(Util.setHandleStringException(characterData.getName(), "NA"));
        tvCharacterHeight.setText(String.format(Util.setHandleStringException(Util.getinCMToMeter(characterData.getHeight()), "NA")));
        tvCharacterMass.setText(characterData.getMass().equalsIgnoreCase("unknown") ? "NA" : Util.setHandleStringException(characterData.getMass(), "NA") + " Kg");
        tvCreatedDate.setText((Util.setHandleStringException(Util.getCharacterDate(characterData.getCreated()), "NA")));
    }


}