package com.arrk.execersice.callBacks;

import org.json.JSONObject;

/**
 * Created by Ashish S. Patil on 11-06-2018.
 */

public interface GetCallBackResponce {
    /*
     *
     * Got Call back Server Responce
     *
     * */
      void getCallBackResponce(int code, String message, String Tag, JSONObject jsonObject);
}
