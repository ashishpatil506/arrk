package com.arrk.execersice.model.character;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class CharacterList implements Serializable
{

    @SerializedName("next")
    @Expose
    private String nextPath;

    public String getNextPath() {
        return nextPath;
    }

    public void setNextPath(String nextPath) {
        this.nextPath = nextPath;
    }

    public ArrayList<CharacterData> getCharacterData() {
        return characterData;
    }

    public void setCharacterData(ArrayList<CharacterData> characterData) {
        this.characterData = characterData;
    }

    @SerializedName("results")
    @Expose
    private ArrayList<CharacterData> characterData=new ArrayList<>();



}
