package com.arrk.execersice.recyclerViewAdapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arrk.execersice.R;
import com.arrk.execersice.activities.CharacterDetailsActivity;
import com.arrk.execersice.model.character.CharacterData;
import com.arrk.execersice.utils.Util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
Binding data to Charcter List
*/
public class CharcterListAdapter extends RecyclerView.Adapter<CharcterListAdapter.MyViewHolder> {

    Context mcontext;
    private ArrayList<CharacterData> characterDataArrayList = new ArrayList<CharacterData>();

    public CharcterListAdapter(Context context, ArrayList<CharacterData> characterDataArrayList) {
        mcontext = context;
        this.characterDataArrayList = characterDataArrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvCharacterName)
        TextView tvCharacterName;
        @BindView(R.id.tvCreatedDate)
        TextView tvCreatedDate;

        View view;


        public MyViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, itemView);
        }


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.charcter_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CharacterData characterData = characterDataArrayList.get(position);
        holder.tvCharacterName.setText(Util.setHandleStringException(characterData.getName(), "NA"));
        holder.tvCreatedDate.setText((Util.setHandleStringException(Util.getCharacterDate(characterData.getCreated()), "NA")));
        holder.view.setId(position);
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mcontext, CharacterDetailsActivity.class);
                intent.putExtra("characterData", characterDataArrayList.get(view.getId()));
                mcontext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return characterDataArrayList.size();
    }
}
