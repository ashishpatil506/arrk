package com.arrk.execersice.restApi;

import android.content.Context;

import com.arrk.execersice.R;
import com.arrk.execersice.callBacks.GetCallBackResponce;
import com.arrk.execersice.interfaces.ApplicationConfig;
import com.arrk.execersice.utils.Loggers;
import com.arrk.execersice.utils.Util;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by Ashish S. Patil on 11-06-2018.
 * Class for to Processing & Fetching data & return server responce data to class
 */

public class APIResponce implements ApplicationConfig {
    //return callback
    GetCallBackResponce getCallBackResponce;
    //identify the api calling by using tag
    String tag;
    //if Server Requiref Headers the we cxan user token
    String token;
    //Server APi url point
    String pathurl;
    //Parameter key & Value
    HashMap<String, Object> body;
    Context context;

    public APIResponce(Context context, @Header("Authorization") String token, @Path("pathurl") String pathurl, String Tag, @Body HashMap<String, Object> body, GetCallBackResponce getCallBackResponce, int methods) {
        this.getCallBackResponce = getCallBackResponce;
        this.tag = Tag;
        this.token = token;
        this.pathurl = pathurl;
        this.body = body;
        this.context = context;
        Loggers.D("pathurl ", "   " + pathurl);
        Loggers.D("pathurl body ", "   " + body);
        //Call method for fetching data
        callAPI(methods);

    }

    //Post Data method
    public void callAPI(int methods) {
        if (Util.isNetworkConnected(context)) {
            //Call method by default POST
            Call<JsonElement> apiCall = APIClient.getInstance().getRestAdapter().getResponce(pathurl, token, body);
            //Call method as per Call
            if (methods == POST)
                apiCall = APIClient.getInstance().getRestAdapter().getResponce(pathurl, token, body);
            else if (methods == GET)
                apiCall = APIClient.getInstance().getRestAdapter().getGetResponce(pathurl, token, body);
            apiCall.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    callBackResponce(response);

                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    getCallBackResponce.getCallBackResponce(-1, t.getMessage(), tag, null);
                }


            });
        } else
            getCallBackResponce.getCallBackResponce(NOINTERNETCONNECTION, context.getString(R.string.nointernetmessage), tag, null);

    }


    //Get Responce Code
    public int getcode(int responceCode) {
        return responceCode;
    }

    /*get Responce Parsing as per Requirement*/
    public void callBackResponce(Response<JsonElement> response) {
        Loggers.D("errorcode ", "   " + response.code());

        Util.printRawResponse(response.errorBody());
        int code = getcode(response.code());
        if (response.body() != null) {
            try {
                JSONObject object = new JSONObject(response.body().toString());
                String message = context.getString(R.string.commonerror);
                if (object.has("message"))
                    message = object.getString("message");
                getCallBackResponce.getCallBackResponce(code, message, tag, object);
            } catch (JSONException e) {
                getCallBackResponce.getCallBackResponce(response.code(), e.getMessage(), tag, null);
            }
        } else if (response.errorBody() != null && response.errorBody().contentLength() > 0) {

            try {
                JSONObject object = new JSONObject(response.errorBody().string());
                String message = context.getString(R.string.commonerror);
                if (object.has("message"))
                    message = object.getString("message");
                getCallBackResponce.getCallBackResponce(code, message, tag, object);
            } catch (Exception e) {
                getCallBackResponce.getCallBackResponce(response.code(), e.getMessage(), tag, null);
            }
        } else
            getCallBackResponce.getCallBackResponce(response.code(), context.getString(R.string.commonerror), tag, null);
    }

}
