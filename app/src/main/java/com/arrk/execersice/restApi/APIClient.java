package com.arrk.execersice.restApi;

import com.arrk.execersice.interfaces.Urls;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Ashish S. Patil on 11-06-2018.
 */
public class APIClient implements Urls {
    private static APIClient instance;

    /*
 get object   access data from server
    */
    public static synchronized APIClient getInstance() {
        if (instance == null) {
            instance = new APIClient();
        }
        return instance;
    }

    /*
//get object of class to access data from server
  */
    public ExecuteAPI getRestAdapter() {
        //set time out time
        final OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(160, TimeUnit.SECONDS)
                .connectTimeout(160, TimeUnit.SECONDS)
                .build();
        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return restAdapter.create(ExecuteAPI.class);
    }
}
