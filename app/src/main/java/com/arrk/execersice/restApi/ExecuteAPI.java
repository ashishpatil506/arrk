package com.arrk.execersice.restApi;


import com.google.gson.JsonElement;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by Ashish S. Patil on 11-06-2018.
 */


public interface ExecuteAPI {
    //All Post API Calling
    @POST
    Call<JsonElement> getResponce(@Url String pathurl, @Header("Authorization") String token, @Body HashMap<String, Object> body);

    //All GET API Calling
    @GET
    Call<JsonElement> getGetResponce(@Url String pathurl, @Header("Authorization") String token, @QueryMap HashMap<String, Object> body);


}