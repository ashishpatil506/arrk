package com.arrk.execersice;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;


/**
 * Created by Ashish S. Patil on 11-06-2018.
 */
public class AppGlobalClass extends MultiDexApplication {


    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);


    }


}



