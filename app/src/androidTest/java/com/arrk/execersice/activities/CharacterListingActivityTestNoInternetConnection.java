package com.arrk.execersice.activities;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.arrk.execersice.R;
import com.arrk.execersice.utils.Util;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class CharacterListingActivityTestNoInternetConnection {

    @Rule
    public ActivityTestRule<CharacterListingActivity> mActivityTestRule = new ActivityTestRule<>(CharacterListingActivity.class);

    //Write code whatever you want before Test Action calling
    @Before
    public void callBefore() {
        //Stop WIFI
        Util.enableDisableWifi(mActivityTestRule.getActivity(), false);
    }

    //Write code whatever you want after Action complted
    @After
    public void callAfter() {
    }

    @Test
    public void characterListingActivityTest2() {

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Call API for got No onternet & Remove Old Data
        mActivityTestRule.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                mActivityTestRule.getActivity().callAPI(true);

            }
        });
        //Start WIFI
        Util.enableDisableWifi(mActivityTestRule.getActivity(), true);
        //Wait for 5 sec to start WIFI
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (Util.isNetworkConnected(mActivityTestRule.getActivity())) {


            ViewInteraction appCompatButton = onView(
                    allOf(withId(R.id.errorretry),
                            isDisplayed()));
            appCompatButton.perform(click());
            ViewInteraction recyclerView = onView(
                    allOf(withId(R.id.recyclerView)));
            recyclerView.perform(actionOnItemAtPosition(0, click()));

            ViewInteraction appCompatImageButton = onView(
                    allOf(withContentDescription("Navigate up"),
                            childAtPosition(
                                    allOf(withId(R.id.toolbar),
                                            childAtPosition(
                                                    withClassName(is("android.widget.LinearLayout")),
                                                    0)),
                                    1),
                            isDisplayed()));
            appCompatImageButton.perform(click());


        }


    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }


}
