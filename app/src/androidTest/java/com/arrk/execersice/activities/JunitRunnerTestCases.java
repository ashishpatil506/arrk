package com.arrk.execersice.activities;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CharacterListingActivityTest.class,
        CharacterListingActivityTestNoInternetConnection.class,
})
public class JunitRunnerTestCases {


}
